package com.example.data.client

import com.example.data.entity.ApiListEnterprise
import com.example.data.entity.ApiUser
import com.example.data.util.RequestHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ApiClient @Inject constructor(
    private val apiService: ApiService
) : RequestHandler() {

    suspend fun singIn(email: String?, password: String?): ApiUser? {
        return withContext(Dispatchers.IO) {
            makeRequest(apiService.signIn(email, password))
        }
    }

    suspend fun searchEnterprises(name: String): ApiListEnterprise? {
        return withContext(Dispatchers.IO) {
            makeRequest(apiService.searchEnterprises(name))
        }
    }
}