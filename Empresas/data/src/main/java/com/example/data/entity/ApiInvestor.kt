package com.example.data.entity


import com.google.gson.annotations.SerializedName

data class ApiInvestor(
    @SerializedName("balance")
    val balance: Double? = null,
    @SerializedName("city")
    val city: String? = null,
    @SerializedName("country")
    val country: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("first_access")
    val firstAccess: Boolean? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("investor_name")
    val investorName: String? = null,
    @SerializedName("photo")
    val photo: String? = null,
    @SerializedName("portfolio")
    val portfolio: ApiPortfolio? = null,
    @SerializedName("portfolio_value")
    val portfolioValue: Double? = null,
    @SerializedName("super_angel")
    val superAngel: Boolean? = null
)