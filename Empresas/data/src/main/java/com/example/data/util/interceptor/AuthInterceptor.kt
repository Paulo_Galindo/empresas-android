package com.example.data.util.interceptor

import com.example.domain.util.Cache
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(private val cache: Cache) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()

        try {
            request.addHeader(ACCESS_TOKEN, cache.getCache(ACCESS_TOKEN, String::class.java))
            request.addHeader(CLIENT, cache.getCache(CLIENT, String::class.java))
            request.addHeader(UID, cache.getCache(UID, String::class.java))
        } finally {

            val response = chain.proceed(request.build())
            response.headers().get(ACCESS_TOKEN)?.let { cache.setCache(ACCESS_TOKEN, it) }
            response.headers().get(CLIENT)?.let { cache.setCache(CLIENT, it) }
            response.headers().get(UID)?.let { cache.setCache(UID, it) }
            return response
        }
    }

    companion object {
        private const val ACCESS_TOKEN = "access-token"
        private const val CLIENT = "client"
        private const val UID = "uid"
    }

}