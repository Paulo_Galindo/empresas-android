package com.example.data.entity


import com.google.gson.annotations.SerializedName

data class ApiListEnterprise(
    @SerializedName("enterprises")
    val enterprises: List<ApiEnterprise>? = null
)