package com.example.data.mappers

import com.example.data.entity.ApiPortfolio
import com.example.data.util.Mapper
import com.example.domain.entity.Portfolio
import javax.inject.Inject

class ApiPortfolioToPortfolioMapper @Inject constructor(

) : Mapper<ApiPortfolio, Portfolio>() {
    override fun transform(t: ApiPortfolio) = Portfolio(
        enterprises = t.enterprises,
        enterprisesNumber = t.enterprisesNumber
    )
}