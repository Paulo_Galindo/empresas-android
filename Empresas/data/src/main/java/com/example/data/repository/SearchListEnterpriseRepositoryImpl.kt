package com.example.data.repository

import com.example.data.client.ApiClient
import com.example.data.entity.ApiListEnterprise
import com.example.data.util.Mapper
import com.example.domain.entity.ListEnterprise
import com.example.domain.repository.SearchListEnterpriseRepository
import javax.inject.Inject

class SearchListEnterpriseRepositoryImpl @Inject constructor(
    private val apiClient: ApiClient,
    private val mapper: Mapper<ApiListEnterprise, ListEnterprise>
) : SearchListEnterpriseRepository {
    override suspend fun searchEnterprises(name: String): ListEnterprise? {
        return apiClient.searchEnterprises(name)?.let(mapper::transform)
    }
}