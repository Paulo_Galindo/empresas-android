package com.example.data.mappers

import com.example.data.entity.ApiEnterprise
import com.example.data.entity.ApiListEnterprise
import com.example.data.util.Mapper
import com.example.domain.entity.Enterprise
import com.example.domain.entity.ListEnterprise
import javax.inject.Inject

class ApiListEnterpriseToListEnterpriseMapper @Inject constructor(
    private val mapper: Mapper<ApiEnterprise, Enterprise>
) : Mapper<ApiListEnterprise, ListEnterprise>() {
    override fun transform(t: ApiListEnterprise) = ListEnterprise(
        enterprises = t.enterprises.let(mapper::transformList)
    )
}