package com.example.data.client

import com.example.data.entity.ApiListEnterprise
import com.example.data.entity.ApiUser
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    suspend fun signIn(
        @Field("email") email: String?,
        @Field("password") password: String?
    ): Response<ApiUser>

    @GET("enterprises")
    suspend fun searchEnterprises(
        @Query("name") name: String
    ): Response<ApiListEnterprise>
}