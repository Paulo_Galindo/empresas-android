package com.example.data.mappers

import com.example.data.entity.ApiEnterprise
import com.example.data.entity.ApiEnterpriseType
import com.example.data.util.Mapper
import com.example.domain.entity.Enterprise
import com.example.domain.entity.EnterpriseType
import javax.inject.Inject

class ApiEnterpriseToEnterpriseMapper @Inject constructor(
    private val mapper: Mapper<ApiEnterpriseType, EnterpriseType>
) : Mapper<ApiEnterprise, Enterprise>() {
    override fun transform(t: ApiEnterprise) = Enterprise(
        city = t.city,
        photo = t.photo,
        id = t.id,
        country = t.country,
        description = t.description,
        emailEnterprise = t.emailEnterprise,
        enterpriseName = t.enterpriseName,
        enterpriseType = t.enterpriseType?.let(mapper::transform),
        facebook = t.facebook,
        linkedin = t.linkedin,
        ownEnterprise = t.ownEnterprise,
        phone = t.phone,
        sharePrice = t.sharePrice,
        twitter = t.twitter,
        value = t.value
    )
}