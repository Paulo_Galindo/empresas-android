package com.example.data.mappers

import com.example.data.entity.ApiInvestor
import com.example.data.entity.ApiUser
import com.example.data.util.Mapper
import com.example.domain.entity.Investor
import com.example.domain.entity.User
import javax.inject.Inject

class ApiUserToUserMapper @Inject constructor(
    private val mapper: Mapper<ApiInvestor, Investor>
) : Mapper<ApiUser, User>() {
    override fun transform(t: ApiUser) = User(
        enterprise = t.enterprise,
        investor = t.investor?.let(mapper::transform),
        success = t.success
    )
}