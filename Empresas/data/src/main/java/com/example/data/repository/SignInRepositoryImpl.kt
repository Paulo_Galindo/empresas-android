package com.example.data.repository

import com.example.data.client.ApiClient
import com.example.data.entity.ApiUser
import com.example.data.util.CURRENT_USER
import com.example.data.util.Mapper
import com.example.domain.entity.User
import com.example.domain.repository.SignInRepository
import com.example.domain.util.Cache
import javax.inject.Inject

class SignInRepositoryImpl @Inject constructor(
    private val apiClient: ApiClient,
    private val mapper: Mapper<ApiUser, User>,
    private val cacheUser: Cache
) : SignInRepository {
    override suspend fun signIn(email: String?, password: String?): User? {
        return apiClient.singIn(email, password)?.let(mapper::transform).also(::saveUser)
    }

    private fun saveUser(user: User?) {
        cacheUser.setCache(CURRENT_USER, user)
    }
}