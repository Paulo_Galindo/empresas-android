package com.example.data.entity


import com.google.gson.annotations.SerializedName

data class ApiPortfolio(
    @SerializedName("enterprises")
    val enterprises: List<Any>? = null,
    @SerializedName("enterprises_number")
    val enterprisesNumber: Int? = null
)