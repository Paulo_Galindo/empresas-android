package com.example.data.entity


import com.google.gson.annotations.SerializedName

data class ApiEnterpriseType(
    @SerializedName("enterprise_type_name")
    val enterpriseTypeName: String? = null,
    @SerializedName("id")
    val id: Int? = null
)