package com.example.data.entity

data class ApiError(
    val message: String?,
    val errors: List<String>? = null
)