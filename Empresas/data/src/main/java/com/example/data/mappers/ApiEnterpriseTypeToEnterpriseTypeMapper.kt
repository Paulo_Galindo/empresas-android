package com.example.data.mappers

import com.example.data.entity.ApiEnterpriseType
import com.example.data.util.Mapper
import com.example.domain.entity.EnterpriseType
import javax.inject.Inject

class ApiEnterpriseTypeToEnterpriseTypeMapper @Inject constructor(

) : Mapper<ApiEnterpriseType, EnterpriseType>() {
    override fun transform(t: ApiEnterpriseType) = EnterpriseType(
        enterpriseTypeName = t.enterpriseTypeName,
        id = t.id
    )
}