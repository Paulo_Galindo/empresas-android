package com.example.data.mappers

import com.example.data.entity.ApiInvestor
import com.example.data.entity.ApiPortfolio
import com.example.data.util.Mapper
import com.example.domain.entity.Investor
import com.example.domain.entity.Portfolio
import javax.inject.Inject

class ApiInvestorToInvestorMapper @Inject constructor(
    private val mapper: Mapper<ApiPortfolio, Portfolio>
) : Mapper<ApiInvestor, Investor>() {
    override fun transform(t: ApiInvestor) = Investor(
        balance = t.balance,
        city = t.city,
        country = t.country,
        email = t.email,
        firstAccess = t.firstAccess,
        id = t.id,
        investorName = t.investorName,
        photo = t.photo,
        portfolio = t.portfolio?.let(mapper::transform),
        portfolioValue = t.portfolioValue,
        superAngel = t.superAngel
    )
}