package com.example.data.util.storage

import android.content.Context
import android.content.SharedPreferences
import com.example.domain.util.Cache
import com.google.gson.Gson
import java.lang.reflect.Type
import javax.inject.Inject

class CacheUser @Inject constructor(): Cache {

    private val gson = Gson()

    override fun setCache(key: String, value: Any?) {
        if (value == null) {
            sharedPreferences.edit().remove(key).apply()
        } else {
            sharedPreferences.edit().putString(key, gson.toJson(value)).apply()
        }
    }

    override fun <T> getCache(key: String, type: Type): T {
        val getValue = sharedPreferences.getString(key, null)
        return gson.fromJson(getValue, type)
    }

    override fun clearCache() {
        sharedPreferences.edit().clear().apply()
    }

    companion object {

        private const val CACHE_USER = "cache_user"

        private lateinit var sharedPreferences: SharedPreferences

        fun init(context: Context) {
            sharedPreferences = context.getSharedPreferences(CACHE_USER, Context.MODE_PRIVATE)
        }
    }
}