package com.example.data.entity


import com.google.gson.annotations.SerializedName

data class ApiUser(
    @SerializedName("enterprise")
    val enterprise: Any? = null,
    @SerializedName("investor")
    val investor: ApiInvestor? = null,
    @SerializedName("success")
    val success: Boolean? = null
)