package com.example.empresas.ui.baseui

import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment() {
    val controller by lazy { findNavController() }
    lateinit var directions: NavDirections

    lateinit var baseViewModel: BaseViewModel
}