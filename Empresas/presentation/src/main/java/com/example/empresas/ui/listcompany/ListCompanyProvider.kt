package com.example.empresas.ui.listcompany

import androidx.lifecycle.ViewModel
import com.example.empresas.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ListCompanyProvider {

    @Binds
    @IntoMap
    @ViewModelKey(ListCompanyViewModel::class)
    abstract fun providesListCompanyViewModel(viewModel: ListCompanyViewModel): ViewModel
}