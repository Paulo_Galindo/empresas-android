package com.example.empresas.ui.listcompany

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.entity.Enterprise
import com.example.empresas.databinding.ItemListEnterpriseBinding

class ListEnterpriseViewHolder(
    private val binding: ItemListEnterpriseBinding,
    private val callbackClick: (Enterprise) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(enterprise: Enterprise) {
        binding.enterprise = enterprise
        binding.cardItem.setOnClickListener {
            callbackClick.invoke(enterprise)
        }
    }

    companion object {
        fun inflate(
            parent: ViewGroup,
            callbackClick: (Enterprise) -> Unit
        ): ListEnterpriseViewHolder {
            return ListEnterpriseViewHolder(
                ItemListEnterpriseBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                callbackClick
            )
        }
    }
}