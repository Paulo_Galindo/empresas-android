package com.example.empresas.ui.enterprisedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.entity.Enterprise
import com.example.empresas.ui.baseui.BaseViewModel
import javax.inject.Inject

class EnterpriseDetailViewModel @Inject constructor(

) : BaseViewModel() {

    val enterprise: LiveData<Enterprise> get() = _enterprise
    private val _enterprise: MutableLiveData<Enterprise> = MutableLiveData()

    internal fun bindEnterprise(enterprise: Enterprise?) {
        _enterprise.value = enterprise
    }
}