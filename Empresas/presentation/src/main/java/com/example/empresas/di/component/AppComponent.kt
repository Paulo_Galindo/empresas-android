package com.example.empresas.di.component

import com.example.empresas.ApplicationApp
import com.example.empresas.di.module.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ApiProviderModule::class,
        ActivityBindingModule::class,
        FragmentBindModule::class,
        ApplicationBindingModule::class,
        MapperModule::class
    ]
)
interface AppComponent : AndroidInjector<ApplicationApp>