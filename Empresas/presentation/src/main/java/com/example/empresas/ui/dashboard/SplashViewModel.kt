package com.example.empresas.ui.dashboard

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.example.data.util.CURRENT_USER
import com.example.domain.entity.User
import com.example.domain.util.Cache
import com.example.empresas.ui.baseui.BaseViewModel
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val cacheUser: Cache
) : BaseViewModel() {

    val goToLogin: LiveData<Boolean> get() = _goToLogin
    private val _goToLogin: MutableLiveData<Boolean> = MutableLiveData()

    private val user: User? = cacheUser.getCache(CURRENT_USER, User::class.java)

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun goTo() {
        if (user == null)
            _goToLogin.postValue(true)
        else
            _goToLogin.postValue(false)
    }
}