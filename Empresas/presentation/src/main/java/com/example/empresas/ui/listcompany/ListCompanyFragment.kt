package com.example.empresas.ui.listcompany

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.entity.Enterprise
import com.example.empresas.R
import com.example.empresas.databinding.FragmentListCompanyBinding
import com.example.empresas.ui.baseui.BaseFragment
import com.example.empresas.util.setupToolbar
import com.example.empresas.util.viewModelProvider
import javax.inject.Inject

class ListCompanyFragment : BaseFragment() {

    private lateinit var binding: FragmentListCompanyBinding
    private lateinit var viewModel: ListCompanyViewModel
    private lateinit var searchView: SearchView
    private val adapterList: ListEnterpriseAdapter = ListEnterpriseAdapter { callbackClick(it) }

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentListCompanyBinding.inflate(inflater, container, false)
        viewModel = viewModelProvider(viewModelFactory)
        setupToolbar(binding.includedToolbar.toolbar)
        setHasOptionsMenu(true)

        subscribeUI()
        setupRecycler()

        return binding.root
    }

    private fun subscribeUI() {
        with(viewModel) {
            listEnterprise.observe(viewLifecycleOwner, Observer { list ->
                adapterList.submitList(list)
            })
            noResults.observe(viewLifecycleOwner, Observer { noResults ->
                binding.noResultsFound.visibility = if (noResults) View.VISIBLE else View.GONE
            })
            error.observe(viewLifecycleOwner, Observer {error ->
                setDialogError(error)
            })
        }
    }

    private fun setDialogError(error: String) {
        AlertDialog
            .Builder(context)
            .setTitle(R.string.error)
            .setMessage(error)
            .setPositiveButton(R.string.global_ok) { _, _ -> }
            .show()
    }

    private fun setupRecycler() {
        with(binding.recycler) {
            adapter = adapterList
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun callbackClick(enterprise: Enterprise) {
        directions = ListCompanyFragmentDirections.actionListCompanyToEnterpriseDetail(enterprise)
        controller.navigate(directions)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val menuItem = menu.findItem(R.id.search_icon)
        searchView = menuItem.actionView as SearchView
        setupSearchView()
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setupSearchView() {
        searchView.queryHint = "Pesquisar"

        searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            binding.clickToSearch.visibility = if (hasFocus) View.GONE else View.VISIBLE
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null && newText.isNotBlank()) {
                    viewModel.search(newText)
                } else {
                    viewModel.cleanList()
                }
                return false
            }
        })
    }
}