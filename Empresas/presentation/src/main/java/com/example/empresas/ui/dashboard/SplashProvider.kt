package com.example.empresas.ui.dashboard

import androidx.lifecycle.ViewModel
import com.example.empresas.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class SplashProvider {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun providesViewModel(viewModel: SplashViewModel): ViewModel
}