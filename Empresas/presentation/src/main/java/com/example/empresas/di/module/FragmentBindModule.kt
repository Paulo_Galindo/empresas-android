package com.example.empresas.di.module

import com.example.empresas.di.scope.FragmentScope
import com.example.empresas.ui.dashboard.SplashFragment
import com.example.empresas.ui.dashboard.SplashProvider
import com.example.empresas.ui.enterprisedetail.EnterpriseDetailFragment
import com.example.empresas.ui.enterprisedetail.EnterpriseDetailProvider
import com.example.empresas.ui.listcompany.ListCompanyFragment
import com.example.empresas.ui.listcompany.ListCompanyProvider
import com.example.empresas.ui.login.LoginFragment
import com.example.empresas.ui.login.LoginProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentBindModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = [LoginProvider::class])
    fun contributeLoginFragment(): LoginFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [ListCompanyProvider::class])
    fun contributesListCompanyFragment(): ListCompanyFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [SplashProvider::class])
    fun contributesSplashFragment(): SplashFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [EnterpriseDetailProvider::class])
    fun contributesEnterpriseDetailFragment(): EnterpriseDetailFragment
}