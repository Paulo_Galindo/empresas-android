package com.example.empresas.ui.listcompany

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.entity.Enterprise
import com.example.domain.usecase.SearchEnterprise
import com.example.empresas.ui.baseui.BaseViewModel
import javax.inject.Inject

class ListCompanyViewModel @Inject constructor(
    private val searchEnterprise: SearchEnterprise
) : BaseViewModel() {

    val listEnterprise: LiveData<List<Enterprise>> get() = _listEnterprise
    private val _listEnterprise: MutableLiveData<List<Enterprise>> = MutableLiveData()

    val noResults: LiveData<Boolean> get() = _noResults
    private val _noResults: MutableLiveData<Boolean> = MutableLiveData()

    val error: LiveData<String> get() = _error
    private val _error: MutableLiveData<String> = MutableLiveData()

    internal fun search(name: String) {
        asyncScope(
            block = { searchEnterprise.execute(name) },
            onSuccess = {
                _listEnterprise.postValue(it?.enterprises)
                _noResults.postValue(it?.enterprises?.isEmpty())
            },
            onFailure = { onFailure(it) }
        )
    }

    internal fun cleanList() {
        _noResults.value = false
        _listEnterprise.value = emptyList()
    }

    private fun onFailure(throwable: Throwable) {
        _error.value = throwable.message
    }
}