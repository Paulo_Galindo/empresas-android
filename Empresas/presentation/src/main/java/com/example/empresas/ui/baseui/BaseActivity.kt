package com.example.empresas.ui.baseui

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {
    lateinit var controller: NavController
    lateinit var directions: NavDirections

    lateinit var baseViewModel: BaseViewModel
}