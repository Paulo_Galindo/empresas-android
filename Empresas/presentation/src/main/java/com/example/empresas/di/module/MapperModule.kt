package com.example.empresas.di.module

import com.example.data.entity.*
import com.example.data.mappers.*
import com.example.data.util.Mapper
import com.example.domain.entity.*
import dagger.Binds
import dagger.Module

@Module
interface MapperModule {

    @Binds
    fun bindsApiUserToUserMapper(mapper: ApiUserToUserMapper): Mapper<ApiUser, User>

    @Binds
    fun bindsApiInvestorToInvestorMapper(
        mapper: ApiInvestorToInvestorMapper
    ): Mapper<ApiInvestor, Investor>

    @Binds
    fun bindsApiPortfolioToPortfolio(
        mapper: ApiPortfolioToPortfolioMapper
    ): Mapper<ApiPortfolio, Portfolio>

    @Binds
    fun bindsApiListEnterpriseToListEnterpriseMapper(
        mapper: ApiListEnterpriseToListEnterpriseMapper
    ): Mapper<ApiListEnterprise, ListEnterprise>

    @Binds
    fun bindsApiEnterpriseToEnterpriseMapper(
        mapper: ApiEnterpriseToEnterpriseMapper
    ): Mapper<ApiEnterprise, Enterprise>

    @Binds
    fun bindsApiEnterpriseTypeToEnterpriseTypeMapper(
        mapper: ApiEnterpriseTypeToEnterpriseTypeMapper
    ): Mapper<ApiEnterpriseType, EnterpriseType>
}