package com.example.empresas.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.usecase.SignIn
import com.example.domain.util.InputException
import com.example.empresas.ui.baseui.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val signIn: SignIn
) : BaseViewModel() {

    val email: LiveData<String> get() = _email
    private val _email: MutableLiveData<String> = MutableLiveData()

    val password: LiveData<String> get() = _password
    private val _password: MutableLiveData<String> = MutableLiveData()

    val loginSuccess: LiveData<Boolean> get() = _loginSuccess
    private val _loginSuccess: MutableLiveData<Boolean> = MutableLiveData()

    val invalidEmail: LiveData<String> get() = _invalidEmail
    private val _invalidEmail: MutableLiveData<String> = MutableLiveData()

    val invalidPassword: LiveData<String> get() = _invalidPassword
    private val _invalidPassword: MutableLiveData<String> = MutableLiveData()

    val error: LiveData<String> get() = _error
    private val _error: MutableLiveData<String> = MutableLiveData()

    val loading: LiveData<Boolean> get() = _loading
    private val _loading: MutableLiveData<Boolean> = MutableLiveData()

    internal fun signIn() {
        asyncScope(
            block = {
                _loading.postValue(true)
                signIn.execute(_email.value, _password.value)
            },
            onSuccess = {
                _loading.postValue(false)
                _loginSuccess.postValue(it?.success)
            },
            onFailure = { onFailure(it) }
        )
    }

    internal fun observeTextEmail(email: String) {
        _email.postValue(email)
    }

    internal fun observeTextPassword(password: String) {
        _password.postValue(password)
    }

    private fun onFailure(throwable: Throwable) {
        if (throwable is InputException) {
            when (throwable.getException().first) {
                InputException.EMAIL_EXCEPTION -> {
                    _invalidEmail.postValue(throwable.getException().second)
                }
                InputException.PASSWORD_EXCEPTION -> {
                    _invalidPassword.postValue(throwable.getException().second)
                }
            }
        } else {
            _error.postValue(throwable.message)
        }
        _loading.postValue(false)
    }


}