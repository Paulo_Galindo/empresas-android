package com.example.empresas.ui.baseui

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    internal fun <T> asyncScope(
        block: suspend () -> T,
        onSuccess: (T) -> Unit,
        onFailure: ((Throwable) -> Unit)? = null
    ) {
        viewModelScope.launch {
            runCatching {
                block()
            }.onSuccess {
                onSuccess(it)
            }.onFailure {
                onFailure?.invoke(it)
            }
        }
    }
}