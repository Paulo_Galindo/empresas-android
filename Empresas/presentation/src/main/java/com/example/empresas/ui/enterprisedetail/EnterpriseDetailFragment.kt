package com.example.empresas.ui.enterprisedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.empresas.R
import com.example.empresas.databinding.FragmentEnterpriseDetailBinding
import com.example.empresas.ui.baseui.BaseFragment
import com.example.empresas.util.setupToolbar
import com.example.empresas.util.viewModelProvider
import javax.inject.Inject

class EnterpriseDetailFragment : BaseFragment() {

    private lateinit var binding: FragmentEnterpriseDetailBinding
    private lateinit var viewModel: EnterpriseDetailViewModel
    private val args by lazy { arguments?.let { EnterpriseDetailFragmentArgs.fromBundle(it) } }

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentEnterpriseDetailBinding.inflate(inflater, container, false)
        viewModel = viewModelProvider(viewModelFactory)
        viewModel.bindEnterprise(args?.enterprise)
        setupToolbar(binding.includedToolbar.toolbar)
        binding.includedToolbar.toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        subscribeUI()
        setClickListener()

        return binding.root
    }

    private fun setClickListener() {
        binding.includedToolbar.toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
    }

    private fun subscribeUI() {
        binding.includedToolbar.imageIoasys.visibility = View.GONE
        viewModel.enterprise.observe(viewLifecycleOwner, Observer {
            binding.enterprise = it
            binding.includedToolbar.toolbar.title = it.enterpriseName
        })
    }
}