package com.example.empresas

import com.example.data.util.storage.CacheUser
import com.example.empresas.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class ApplicationApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        CacheUser.init(this)
    }

    override fun applicationInjector(): AndroidInjector<out ApplicationApp> {
        return DaggerAppComponent.create()
    }
}