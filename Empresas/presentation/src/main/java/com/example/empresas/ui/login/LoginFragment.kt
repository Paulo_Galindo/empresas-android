package com.example.empresas.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.empresas.databinding.FragmentLoginBinding
import com.example.empresas.ui.baseui.BaseFragment
import com.example.empresas.util.viewModelProvider
import javax.inject.Inject

class LoginFragment : BaseFragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var viewModel: LoginViewModel

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        viewModel = viewModelProvider(viewModelFactory)
        lifecycle.addObserver(viewModel)
        subscribeUI()
        setClickListeners()

        return binding.root
    }

    private fun subscribeUI() {
        with(binding) {
            editTextEmail.addTextChangedListener { text ->
                viewModel.observeTextEmail(text.toString())
            }

            editTextPassword.addTextChangedListener { text ->
                viewModel.observeTextPassword(text.toString())
            }
        }

        with(viewModel) {
            invalidEmail.observe(viewLifecycleOwner, Observer {
                binding.layoutTextEmail.error = it
            })

            invalidPassword.observe(viewLifecycleOwner, Observer {
                binding.layoutTextPassword.error = it
            })

            error.observe(viewLifecycleOwner, Observer {
                binding.layoutTextPassword.error = it
            })

            loginSuccess.observe(viewLifecycleOwner, Observer {
                if (it) {
                    directions = LoginFragmentDirections.actionLoginToListCompany()
                    controller.navigate(directions)
                }
            })

            loading.observe(viewLifecycleOwner, Observer { loading ->
                if (loading) {
                    binding.scrollViewLogin.alpha = 0.6f
                    binding.progressLoading.visibility = View.VISIBLE
                } else {
                    binding.scrollViewLogin.alpha = 1f
                    binding.progressLoading.visibility = View.GONE
                }
            })
        }
    }

    private fun setClickListeners() {
        binding.buttonSubmit.setOnClickListener {
            viewModel.signIn()
        }
    }
}