package com.example.empresas.ui.dashboard

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.postDelayed
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.empresas.databinding.FragmentSplashBinding
import com.example.empresas.ui.baseui.BaseFragment
import com.example.empresas.util.viewModelProvider
import javax.inject.Inject

class SplashFragment : BaseFragment() {

    private lateinit var binding: FragmentSplashBinding
    private lateinit var viewModel: SplashViewModel
    private val handler = Handler()

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentSplashBinding.inflate(inflater, container, false)

        viewModel = viewModelProvider(viewModelFactory)
        lifecycle.addObserver(viewModel)
        handler.postDelayed(1000) {
            subscribeUI()
        }

        return binding.root
    }

    private fun subscribeUI() {
        viewModel.goToLogin.observe(viewLifecycleOwner, Observer { goToLogin ->
            directions = if (goToLogin) {
                SplashFragmentDirections.actionSplashToLogin()
            } else {
                SplashFragmentDirections.actionSplashToListCompany()
            }
            controller.navigate(directions)
        })
    }
}