package com.example.empresas.ui.dashboard

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.empresas.R
import com.example.empresas.databinding.ActivityDashboardBinding
import com.example.empresas.ui.baseui.BaseActivity

class DashBoardActivity : BaseActivity() {

    private lateinit var binding: ActivityDashboardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
    }
}
