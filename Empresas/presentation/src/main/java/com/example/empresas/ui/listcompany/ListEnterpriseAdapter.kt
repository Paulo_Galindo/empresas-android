package com.example.empresas.ui.listcompany

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.domain.entity.Enterprise

class ListEnterpriseAdapter(
    private val callbackClick: (Enterprise) -> Unit
) : ListAdapter<Enterprise, ListEnterpriseViewHolder>(DIFF_UTIL) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListEnterpriseViewHolder {
        return ListEnterpriseViewHolder.inflate(parent, callbackClick)
    }

    override fun onBindViewHolder(holder: ListEnterpriseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<Enterprise>() {
            override fun areItemsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean {
                return oldItem == newItem
            }

        }
    }
}