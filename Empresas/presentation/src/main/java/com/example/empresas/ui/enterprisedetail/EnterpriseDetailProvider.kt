package com.example.empresas.ui.enterprisedetail

import androidx.lifecycle.ViewModel
import com.example.empresas.di.scope.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class EnterpriseDetailProvider {

    @Binds
    @IntoMap
    @ViewModelKey(EnterpriseDetailViewModel::class)
    abstract fun providesEnterpriseDetailViewModel(viewModel: EnterpriseDetailViewModel): ViewModel
}