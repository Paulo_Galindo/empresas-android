package com.example.empresas.di.module

import com.example.empresas.di.scope.ActivityScope
import com.example.empresas.ui.dashboard.DashBoardActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    fun contributesMainActivity(): DashBoardActivity
}