package com.example.empresas.di.module

import androidx.lifecycle.ViewModelProvider
import com.example.data.repository.SearchListEnterpriseRepositoryImpl
import com.example.data.repository.SignInRepositoryImpl
import com.example.data.util.interceptor.AuthInterceptor
import com.example.data.util.storage.CacheUser
import com.example.domain.repository.SearchListEnterpriseRepository
import com.example.domain.repository.SignInRepository
import com.example.domain.util.Cache
import com.example.empresas.ui.baseui.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import okhttp3.Interceptor

@Module
interface ApplicationBindingModule {

    @Binds
    fun bindsCacheUser(cacheUser: CacheUser): Cache

    @Binds
    @IntoSet
    fun bindsAuthInterceptor(interceptor: AuthInterceptor): Interceptor

    @Binds
    fun bindsSignInRepositoryImpl(repositoryImpl: SignInRepositoryImpl): SignInRepository

    @Binds
    fun bindsSearchListEnterpriseRepositoryImpl(repository: SearchListEnterpriseRepositoryImpl): SearchListEnterpriseRepository

    @Binds
    fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}