package com.example.domain.entity

data class EnterpriseType(
    val enterpriseTypeName: String? = null,
    val id: Int? = null
)