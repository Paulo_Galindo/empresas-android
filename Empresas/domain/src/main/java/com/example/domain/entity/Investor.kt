package com.example.domain.entity

data class Investor(
    val balance: Double? = null,
    val city: String? = null,
    val country: String? = null,
    val email: String? = null,
    val firstAccess: Boolean? = null,
    val id: Int? = null,
    val investorName: String? = null,
    val photo: String? = null,
    val portfolio: Portfolio? = null,
    val portfolioValue: Double? = null,
    val superAngel: Boolean? = null
)