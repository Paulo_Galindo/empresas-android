package com.example.domain.util

import java.security.InvalidParameterException

class InputException: InvalidParameterException() {

    private lateinit var input: Pair<Int, String>

    fun getException(): Pair<Int, String> {
        return input
    }

    fun addException(typeError: Int, message: String) {
        input = typeError to message
    }

    companion object {
        const val EMAIL_EXCEPTION = 0
        const val PASSWORD_EXCEPTION = 1
    }
}