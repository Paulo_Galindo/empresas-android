package com.example.domain.entity

data class User(
    val enterprise: Any? = null,
    val investor: Investor? = null,
    val success: Boolean? = null
)