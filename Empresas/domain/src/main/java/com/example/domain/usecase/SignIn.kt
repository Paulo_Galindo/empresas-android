package com.example.domain.usecase

import com.example.domain.entity.User
import com.example.domain.repository.SignInRepository
import com.example.domain.util.EMAIL_REGEX
import com.example.domain.util.INVALID_EMAIL
import com.example.domain.util.INVALID_PASSWORD
import com.example.domain.util.InputException
import javax.inject.Inject

class SignIn @Inject constructor(
    private val repository: SignInRepository
) {

    suspend fun execute(email: String?, password: String?) = validateInputs(email, password)

    private suspend fun validateInputs(email: String?, password: String?): User? {
        if (email != null) {
            if (email.matches(EMAIL_REGEX.toRegex()) && !password.isNullOrEmpty()) {
                return repository.signIn(email, password)
            } else {
                if (!email.matches(EMAIL_REGEX.toRegex()))
                    throw InputException().apply {
                        addException(
                            InputException.EMAIL_EXCEPTION,
                            INVALID_EMAIL
                        )
                    }
                else
                    throw InputException().apply {
                        addException(
                            InputException.PASSWORD_EXCEPTION,
                            INVALID_PASSWORD
                        )
                    }
            }
        } else {
            throw InputException().apply {
                addException(
                    InputException.EMAIL_EXCEPTION,
                    INVALID_EMAIL
                )
            }
        }
    }
}