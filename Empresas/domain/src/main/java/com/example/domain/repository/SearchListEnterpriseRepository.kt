package com.example.domain.repository

import com.example.domain.entity.ListEnterprise

interface SearchListEnterpriseRepository {
    suspend fun searchEnterprises(name: String): ListEnterprise?
}