package com.example.domain.util

import java.lang.reflect.Type

interface Cache {
    fun setCache(key: String, value: Any?)
    fun <T> getCache(key: String, type: Type): T
    fun clearCache()
}