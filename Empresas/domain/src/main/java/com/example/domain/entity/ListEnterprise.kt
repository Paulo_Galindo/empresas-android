package com.example.domain.entity

data class ListEnterprise(
    val enterprises: List<Enterprise>? = null
)