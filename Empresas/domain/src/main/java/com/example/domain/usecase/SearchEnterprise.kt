package com.example.domain.usecase

import com.example.domain.repository.SearchListEnterpriseRepository
import javax.inject.Inject

class SearchEnterprise @Inject constructor(
    private val repository: SearchListEnterpriseRepository
) {
    suspend fun execute(name: String) = repository.searchEnterprises(name)
}