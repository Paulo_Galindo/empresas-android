package com.example.domain.entity

data class Portfolio(
    val enterprises: List<Any>? = null,
    val enterprisesNumber: Int? = null
)