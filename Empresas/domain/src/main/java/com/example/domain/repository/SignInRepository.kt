package com.example.domain.repository

import com.example.domain.entity.User

interface SignInRepository {
    suspend fun signIn(email: String?, password: String?): User?
}