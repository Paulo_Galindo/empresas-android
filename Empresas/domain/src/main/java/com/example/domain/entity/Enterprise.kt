package com.example.domain.entity

import java.io.Serializable

data class Enterprise(
    val city: String? = null,
    val country: String? = null,
    val description: String? = null,
    val emailEnterprise: String? = null,
    val enterpriseName: String? = null,
    val enterpriseType: EnterpriseType? = null,
    val facebook: String? = null,
    val id: Int? = null,
    val linkedin: String? = null,
    val ownEnterprise: Boolean? = null,
    val phone: String? = null,
    val photo: String? = null,
    val sharePrice: Double? = null,
    val twitter: String? = null,
    val value: Int? = null
) : Serializable