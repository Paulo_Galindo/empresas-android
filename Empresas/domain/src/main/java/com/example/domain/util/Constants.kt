package com.example.domain.util

const val EMAIL_REGEX = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"
const val INVALID_EMAIL = "Email inválido!"
const val INVALID_PASSWORD = "Campo vazio!"